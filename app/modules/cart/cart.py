from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from ..forms import add_to_cart_form
from flask import request
from decimal import Decimal



mod = Blueprint('cart', __name__)



@mod.route('/cart_items')
def list_cart_items():
    cart = g.cartdb.get_cart_items(session['username'])
    total = g.cartdb.get_total(session['username'])
    return render_template('cart/cart_items_list.html', cart=cart, total = total)

@mod.route('/cart_form', methods=['POST'])
def go_to_add_form():
    form = add_to_cart_form.AddToCartForm(request.form)
    item = request.form['item']
    price = request.form['price']
    session['item'] = item
    session['price'] = price
    return render_template('cart/add_to_cart.html', form=form, item=item, price=price)



@mod.route('/add_to_cart', methods=['POST'])
def add_to_cart():
	form = add_to_cart_form.AddToCartForm(request.form)
	item = session.get('item', None)
	price = session.get('price', None)
	if form.validate():
		quantity = form.quantity.data
		g.cartdb.add_item_to_cart(item, float(price), quantity, session['username'])
		return redirect('/cart/cart_items')
	return render_template('cart/add_to_cart.html', form=form , item = item, price = price)



@mod.route('/remove', methods=['POST'])
def remove():
	g.cartdb.remove()
	g.inventorydb.remove()
	cart = g.cartdb.get_cart_items(session['username'])
	return render_template('cart/cart_items_list.html', cart=cart)

	