from bson.objectid import ObjectId
from decimal import Decimal

class CartDB:
    def __init__(self, conn):
        self.conn = conn

    # def get_cart_items(self, username):
    #     return self.conn.find({'username':username})

    def add_item_to_cart(self, item, price, quantity, username):
        self.conn.insert({'item':item, 'price': price, 'quantity': quantity, 'username' : username})


    def get_cart_items(self, username):
        return self.conn.aggregate( [ {"$match": { "username" : username} } , { "$group": { "_id": "$item", "quantity": {"$sum": "$quantity"}, "total_price": { "$sum": {"$multiply": ["$price", "$quantity"]}}, "price": {"$first": "$price"} } } , { "$lookup": { "from": "inventory", "localField": "_id", "foreignField": "item", "as": "item_details" } }  ] ) 

    def get_total(self, username):
        return self.conn.aggregate([{"$match": {"username" : username}} , { "$group": { "_id": "$username", "total": { "$sum": {"$multiply": ["$price", "$quantity"]}}}}])

    def remove(self):
        self.conn.remove({})






    # def removePost(self, _id):
    # 	self.conn.remove({'_id': ObjectId(_id)})

    # def updatePost(self, _id, emotion, username, post):
    #     self.conn.update({'_id' : ObjectId(_id) },{'$set': {'emotion': emotion}} );

        




