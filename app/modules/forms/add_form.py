from wtforms import Form
from wtforms import TextField
from wtforms import validators
from wtforms import FloatField

class AddItemForm(Form):
    item = TextField('Item', [validators.Required('Please give a valid item name')])
    price = FloatField('Price', [validators.Required('Please give a valid amount')])