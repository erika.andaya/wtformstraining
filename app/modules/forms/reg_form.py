from wtforms import Form
from wtforms import BooleanField
from wtforms import TextField
from wtforms import PasswordField
from wtforms import validators



class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=25 , message = "Please give a valid username")])
    email = TextField('Email', [validators.Email()])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')



