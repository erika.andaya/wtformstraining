from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from ..forms import add_form
from ..forms import add_to_cart_form


mod = Blueprint('inventory', __name__)

@mod.route('/items')
def list_items():
	form = add_to_cart_form.AddToCartForm(request.form)
	inventory = g.inventorydb.get_items()
	return render_template('inventory/items_list.html', inventory=inventory, form=form)

@mod.route('/add_item', methods=['GET', 'POST'])
def add_item():
	form = add_form.AddItemForm(request.form)
	if request.method == 'POST' and form.validate():
		item = form.item.data
		price = form.price.data
		g.inventorydb.add_item(item, price)
		return redirect('/inventory/items')
	return render_template('inventory/add_item.html', form=form)





	#if action == 'add':
	#	new_post = request.form['new_post']
	#	g.postsdb.createPost(new_post, session['username'])
	#	flash('New post created!', 'create_post_success')


	#elif action == 'remove':
	#	post_id = str(request.form['id'])
	#	g.postsdb.removePost(post_id)
	#	flash('Post Deleted!', 'create_post_success')
	#else:
	#	post = request.form['post']
	#	post_id = str(request.form['id'])
	#	g.postsdb.updatePost(post_id, action, session['username'], post)
	#	flash('Emotion Added!', 'create_post_success')


	#return redirect(url_for('.post_list'))

	


	